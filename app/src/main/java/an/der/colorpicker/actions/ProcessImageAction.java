package an.der.colorpicker.actions;

import android.graphics.Bitmap;

public class ProcessImageAction {

    private Bitmap bitmap;

    public ProcessImageAction(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }
}