package an.der.colorpicker;

import an.der.colorpicker.actions.ProcessImageAction;
import an.der.colorpicker.exception.CameraNotFoundException;
import an.der.colorpicker.util.CameraProvider;
import an.der.colorpicker.view.CameraPreview;
import android.app.Activity;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.hardware.Camera;
import android.os.Bundle;
import android.widget.FrameLayout;
import android.widget.TextView;
import de.greenrobot.event.EventBus;


public class MainActivity extends Activity {

    private static final int PIXEL_DELTA = 5;
    private Camera mCamera;
    private CameraPreview mPreview;
    private FrameLayout preview;
    private boolean inPreview;
    private TextView label;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_portrait);
        preview = (FrameLayout) findViewById(R.id.camera_preview);
        label = (TextView) findViewById(R.id.button_capture);
        createCamera();
    }

    private void createCamera() {
        try {
            mCamera = CameraProvider.getCameraInstance();
        } catch (CameraNotFoundException e) {
            e.printStackTrace();
            // ToDo set error message screen
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        initCameraView();
    }

    private void initCameraView() {
        setOrientationConfig();
    }

    private void setOrientationConfig() {
        if (preview != null && mPreview != null) {
            preview.removeView(mPreview);
        }

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            setContentView(R.layout.activity_main_landscape);
        } else {
            setContentView(R.layout.activity_main_portrait);
        }
        CameraProvider.setCameraDisplayOrientation(this, 0, mCamera);
        mPreview = new CameraPreview(this, mCamera);
        preview = (FrameLayout) findViewById(R.id.camera_preview);
        preview.addView(mPreview);
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
        if (mCamera == null) {
            createCamera();
        }
        initCameraView();
    }

    @Override
    protected void onPause() {
        releaseCamera();
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @SuppressWarnings("unused")
    public void onEvent(ProcessImageAction action) {
        label = (TextView) findViewById(R.id.button_capture);
        Bitmap bitmap = action.getBitmap();
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();

        int centerX = width / 2;
        int centerY = height / 2;

        int alpha = 0;
        int red = 0;
        int green = 0;
        int blue = 0;
        int counter = 0;

        int currentX = centerX - PIXEL_DELTA;
        int currentY;

        for (int i = 0; i <= PIXEL_DELTA; i++) {
            currentX = currentX + i;
            currentY = centerY - PIXEL_DELTA;
            for (int j = 0; j <= PIXEL_DELTA; j++) {
                currentY = currentY + j;
                int mColor = bitmap.getPixel(currentX, currentY);
                alpha = alpha + Color.alpha(mColor);
                red = red + Color.red(mColor);
                green = green + Color.green(mColor);
                blue = blue + Color.blue(mColor);
                counter++;
            }
        }

        int averagedAlpha = alpha / counter;
        int averagedRed = red / counter;
        int averagedGreen = green / counter;
        int averagedBlue = blue / counter;

        label.setText(String.format("alpha - %d\nred - %d\ngreen - %d\nblue - %d",
                averagedAlpha, averagedRed, averagedGreen, averagedBlue));
        label.setTextColor(Color.argb(averagedAlpha, averagedRed, averagedGreen, averagedBlue));
    }

    private void releaseCamera() {
        if (inPreview) {
            mCamera.stopPreview();
        }
        if (mCamera != null) {
            mCamera.setPreviewCallback(null);
            mCamera.release();
            mCamera = null;
            inPreview = false;
        }
    }
}