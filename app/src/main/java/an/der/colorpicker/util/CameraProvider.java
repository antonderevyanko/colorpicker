package an.der.colorpicker.util;

import an.der.colorpicker.exception.CameraNotFoundException;
import android.app.Activity;
import android.hardware.Camera;
import android.view.Surface;

/**
 * A safe way to get an instance of the Camera object.
 */
public class CameraProvider {

    public static Camera getCameraInstance() throws CameraNotFoundException {
        Camera camera;
        try {
            camera = Camera.open();
        } catch (Exception e) {
            throw new CameraNotFoundException();
        }
        return camera;
    }

    public static void setCameraDisplayOrientation(Activity activity, int cameraId, Camera camera) {
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(cameraId, info);
        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;
            case Surface.ROTATION_90:
                degrees = 90;
                break;
            case Surface.ROTATION_180:
                degrees = 180;
                break;
            case Surface.ROTATION_270:
                degrees = 270;
                break;
        }
        int result = (info.orientation - degrees + 360) % 360;
        /**
         NO NEED TO SET FACING CAMERA
         int result;
         if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
         result = (info.orientation + degrees) % 360;
         result = (360 - result) % 360;  // compensate the mirror
         } else {  // back-facing
         result = (info.orientation - degrees + 360) % 360;
         }
         */
        camera.setDisplayOrientation(result);
    }

}